package com.example.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@GetMapping("/{firstName}/{lastName}")
	public String greeting(@PathVariable String firstName, @PathVariable String lastName) {
		return "Greetings " + firstName + " " + lastName + " from Spring Boot!";
	}
}
